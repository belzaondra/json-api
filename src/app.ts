import express from "express";

const main = () => {
  const app = express();

  app.get("/", (req, res) => {
    res.json({ test: "this is test object" });
  });

  app.listen(4000, () => {
    console.log("server is listening on port 4000");
  });
};

main();
